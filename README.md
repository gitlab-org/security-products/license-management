# GitLab License Management

This project has moved to [https://gitlab.com/gitlab-org/security-products/analyzers/license-finder](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder)
